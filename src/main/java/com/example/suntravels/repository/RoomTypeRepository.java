package com.example.suntravels.repository;

import com.example.suntravels.model.Hotel;
import com.example.suntravels.model.RoomType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RoomTypeRepository extends JpaRepository<RoomType,UUID>
{
    List<RoomType> findAllByHotel( Hotel hotel );
}
