package com.example.suntravels.repository;

import com.example.suntravels.model.Contract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public interface ContractRepository extends JpaRepository<Contract,UUID>
{
//    List<Contract> queryAllByEndDateAfter(Date date);
}
