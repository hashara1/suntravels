package com.example.suntravels.repository;

import com.example.suntravels.model.Contract;
import com.example.suntravels.model.ContractsRoomTypesKey;
import com.example.suntravels.model.ContractsRoomTypesRelation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ContractsRoomTypesRelationRepository extends JpaRepository<ContractsRoomTypesRelation,ContractsRoomTypesKey>
{
    List<ContractsRoomTypesRelation> findAllByContract( Contract contract);

    List<ContractsRoomTypesRelation> queryAllByContract_EndDateAfterOrderByContract_Hotel_HotelIdAscRoomType_MaxAdultsAsc( Date date);



}
