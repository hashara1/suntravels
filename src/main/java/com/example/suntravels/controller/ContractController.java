package com.example.suntravels.controller;

import com.example.suntravels.dto.ContractDTO;
import com.example.suntravels.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@RestController
@RequestMapping("/api/contract")
@CrossOrigin
public class ContractController
{
    @Autowired
    private ContractService contractService;

    @GetMapping("/all")
    public List<ContractDTO> getAllContracts() {
        return contractService.getAllContracts();
    }

    @PostMapping(value ="/add",consumes = "application/json", produces = "application/json")
    public void createHotel( @RequestBody ContractDTO contractDTO ){
        contractService.addContract( contractDTO );
    }

}
