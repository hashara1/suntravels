package com.example.suntravels.controller;

import com.example.suntravels.dto.HotelDTO;
import com.example.suntravels.dto.RoomAdultsForSearchInputDTO;
import com.example.suntravels.dto.SearchInputDTO;
import com.example.suntravels.model.ContractsRoomTypesRelation;
import com.example.suntravels.service.ContractsRoomTypesService;
import com.example.suntravels.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

@RestController
@RequestMapping("/api/hotel")
@CrossOrigin
public class HotelController
{
    @Autowired
    private HotelService hotelService;

    @Autowired
    private ContractsRoomTypesService contractsRoomTypesService;

    @GetMapping("/all")
    public List<HotelDTO> getAllHotels() {
        return hotelService.getAllHotels();
    }

    @PostMapping(value = "/room", consumes = "application/json", produces = "application/json")
    public List<?> getAll( @RequestBody SearchInputDTO searchInputDTO  ){

        Calendar calendar = Calendar.getInstance();
        calendar.setTime( searchInputDTO.getCheckingDate() );
        calendar.add( calendar.DATE,searchInputDTO.getNoOfNights() );
        Date lastDate = calendar.getTime();

        ListIterator<RoomAdultsForSearchInputDTO> iterator
                = searchInputDTO.getRoomsAdults().listIterator();

        List<Integer> numberOfAdults = new ArrayList<>();
        Collections.sort(numberOfAdults);
        while(iterator.hasNext()){
            RoomAdultsForSearchInputDTO roomAdultsForSearchInputDTO
                    = iterator.next();

            numberOfAdults.add( roomAdultsForSearchInputDTO.getNumberOfAdults() );
        }

        return contractsRoomTypesService.getAllActive( lastDate, numberOfAdults );
    }
}
