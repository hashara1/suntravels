package com.example.suntravels.model;

import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "room_types")
public class RoomType
{


    @Id
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID roomTypeId = new UUID( 0,0 );

    @Column(name = "type", nullable = false)
    private String type;


    @Column(name = "max_adults", nullable = false)
    private int maxAdults;

    @ManyToOne(
            cascade = CascadeType.ALL
    )
    private Hotel hotel;

    public UUID getRoomTypeId()
    {
        return roomTypeId;
    }

    public void setRoomTypeId( UUID roomTypeId )
    {
        this.roomTypeId = roomTypeId;
    }

    public Hotel getHotel()
    {
        return hotel;
    }

    public void setHotel( Hotel hotel )
    {
        this.hotel = hotel;
    }

    public String getType()
    {
        return type;
    }

    public void setType( String type )
    {
        this.type = type;
    }

    public int getMaxAdults()
    {
        return maxAdults;
    }

    public void setMaxAdults( int maxAdults )
    {
        this.maxAdults = maxAdults;
    }

    @Override
    public String toString()
    {
        return "{ roomTypeId: " + getRoomTypeId() + ", type: " + getType()
                       +", maxAdults: " + getMaxAdults() + " }" ;
    }


}
