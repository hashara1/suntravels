package com.example.suntravels.model;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
public class ContractsRoomTypesKey implements Serializable
{
    @Type(type="org.hibernate.type.UUIDCharType")
    @Column(name = "contract_id")
    UUID contractId;

    @Type(type="org.hibernate.type.UUIDCharType")
    @Column(name = "room_type_id")
    UUID roomTypeId;


    public UUID getContractId()
    {
        return contractId;
    }

    public void setContractId( UUID contractId )
    {
        this.contractId = contractId;
    }

    public ContractsRoomTypesKey( UUID contractId, UUID roomTypeId )
    {
        this.contractId = contractId;
        this.roomTypeId = roomTypeId;
    }

    public UUID getRoomTypeId()
    {
        return roomTypeId;
    }

    public void setRoomTypeId( UUID roomTypeId )
    {
        this.roomTypeId = roomTypeId;
    }

    public ContractsRoomTypesKey()
    {
    }
}
