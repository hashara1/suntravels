package com.example.suntravels.model;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import java.util.UUID;

@Entity
@Table(name = "hotels")
public class Hotel
{
    @Id
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID hotelId = new UUID( 0,0 );

    @Column(name = "hotel_type", nullable = false)
    private String type;

    @Column(name = "hotel_name", nullable = false)
    private String name;

    @Column(name = "location", nullable = false)
    private String location;

    @Length(max = 10)
    @Column(name = "telephone", nullable = false)
    private String telephone;

    @Email
    @Column(name = "email", nullable = false)
    private String email;

    public String getType()
    {
        return type;
    }

    public void setType( String type )
    {
        this.type = type;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public UUID getHotelId()
    {
        return hotelId;
    }

    public void setHotelId( UUID hotelId )
    {
        this.hotelId = hotelId;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation( String location )
    {
        this.location = location;
    }

    public String getTelephone()
    {
        return telephone;
    }

    public void setTelephone( String telephone )
    {
        this.telephone = telephone;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail( String email )
    {
        this.email = email;
    }

}
