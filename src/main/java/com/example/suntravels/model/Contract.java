package com.example.suntravels.model;

import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "contracts")
public class Contract
{
    @Id
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID contractId;



    @ManyToOne(fetch = FetchType.EAGER, optional = false,cascade= CascadeType.ALL)
    @JoinColumn(name = "hotel_id", nullable = false)
    private Hotel hotel;

    @Column(name = "start_date", nullable = false)
    private Date startDate;

    @Column(name = "end_date", nullable = false)
    private Date endDate;

    @Column(name = "markup")
    private double markup;

    public double getMarkup()
    {
        return markup;
    }

    public void setMarkup( double markup )
    {
        this.markup = markup;
    }

    public void setContractId( UUID contractId )
    {
        this.contractId = contractId;
    }

    public UUID getContractId()
    {
        return contractId;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate( Date startDate )
    {
        this.startDate = startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate( Date endDate )
    {
        this.endDate = endDate;
    }

    public Hotel getHotel()
    {
        return hotel;
    }

    public void setHotel( Hotel hotel )
    {
        this.hotel = hotel;
    }

}
