package com.example.suntravels.model;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity
public class ContractsRoomTypesRelation
{
    @EmbeddedId
    private ContractsRoomTypesKey id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false,cascade= CascadeType.ALL)
    @MapsId("contractId")
    @JoinColumn(name = "contract_id")
    private Contract contract;

    @ManyToOne(fetch = FetchType.LAZY, optional = false,cascade= CascadeType.ALL)
    @MapsId("roomTypeId")
    @JoinColumn(name = "room_type_id")
    private RoomType roomType;

    @Column(name = "price", nullable = false)
    private float price;

    @Column(name = "no_rooms", nullable = false)
    private int noRooms;



    public ContractsRoomTypesRelation( ContractsRoomTypesKey id, Contract contract, RoomType roomType, float price, int noRooms )
    {
        this.id = id;
        this.contract = contract;
        this.roomType = roomType;
        this.price = price;
        this.noRooms = noRooms;
    }

    public ContractsRoomTypesRelation()
    {
    }

    public ContractsRoomTypesKey getId()
    {
        return id;
    }

    public void setId( ContractsRoomTypesKey id )
    {
        this.id = id;
    }

    public Contract getContract()
    {
        return contract;
    }

    public void setContract( Contract contract )
    {
        this.contract = contract;
    }

    public RoomType getRoomType()
    {
        return roomType;
    }

    public void setRoomType( RoomType roomType )
    {
        this.roomType = roomType;
    }

    public float getPrice()
    {
        return price;
    }

    public void setPrice( float price )
    {
        this.price = price;
    }

    public int getNoRooms()
    {
        return noRooms;
    }

    public void setNoRooms( int noRooms )
    {
        this.noRooms = noRooms;
    }

}
