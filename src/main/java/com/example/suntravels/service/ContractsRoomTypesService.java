package com.example.suntravels.service;

import com.example.suntravels.dto.RoomAdultsForSearchInputDTO;
import com.example.suntravels.dto.RoomTypeForSearchResultDTO;
import com.example.suntravels.dto.SearchResultDTO;
import com.example.suntravels.model.ContractsRoomTypesRelation;
import com.example.suntravels.model.Hotel;
import com.example.suntravels.repository.ContractsRoomTypesRelationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import static com.example.suntravels.Constants.MAX_ADULTS;

@Service
public class ContractsRoomTypesService
{
    @Autowired
    private ContractsRoomTypesRelationRepository contractsRoomTypesRelationRepository;

    public List<SearchResultDTO> getAllActive( Date date, List<Integer> numberOfAdults){
        List<ContractsRoomTypesRelation> contractsRoomTypesRelations =  contractsRoomTypesRelationRepository.queryAllByContract_EndDateAfterOrderByContract_Hotel_HotelIdAscRoomType_MaxAdultsAsc( date );

        ListIterator<ContractsRoomTypesRelation> iterator = contractsRoomTypesRelations.listIterator();

        List<SearchResultDTO> isPossible = updateIsPossible(contractRoomTypesForSearchResultDTO(iterator),numberOfAdults,true);

        return isPossible;
    }

    private List<SearchResultDTO> contractRoomTypesForSearchResultDTO(ListIterator<ContractsRoomTypesRelation> iterator){
        List<SearchResultDTO> searchResultDTOS = new ArrayList<>();
        Hotel previousHotel = new Hotel();

        while(iterator.hasNext()){
            ContractsRoomTypesRelation contractsRoomTypesRelation
                    = iterator.next();

            if (previousHotel.equals( contractsRoomTypesRelation.getContract().getHotel() )){
                SearchResultDTO searchResultDTO = searchResultDTOS.get( searchResultDTOS.size() -1 );

                RoomTypeForSearchResultDTO roomTypeForSearchResultDTO
                        = new RoomTypeForSearchResultDTO();
                roomTypeForSearchResultDTO.setContractsRoomTypesKey( contractsRoomTypesRelation.getId() );
                roomTypeForSearchResultDTO.setMaxAdults( contractsRoomTypesRelation.getRoomType().getMaxAdults() );
                roomTypeForSearchResultDTO.setNoRooms( contractsRoomTypesRelation.getNoRooms() );
                roomTypeForSearchResultDTO.setPrice( (contractsRoomTypesRelation.getPrice() * (1+ ((float)contractsRoomTypesRelation.getContract().getMarkup())/100)) );
                roomTypeForSearchResultDTO.setType( contractsRoomTypesRelation.getRoomType().getType() );

                searchResultDTO.addRoom( roomTypeForSearchResultDTO );

            }
            else{
                SearchResultDTO searchResultDTO = new SearchResultDTO();
                searchResultDTO.setHotel( contractsRoomTypesRelation.getContract().getHotel() );
                previousHotel = contractsRoomTypesRelation.getContract().getHotel();

                RoomTypeForSearchResultDTO roomTypeForSearchResultDTO
                        = new RoomTypeForSearchResultDTO();
                roomTypeForSearchResultDTO.setContractsRoomTypesKey( contractsRoomTypesRelation.getId() );
                roomTypeForSearchResultDTO.setMaxAdults( contractsRoomTypesRelation.getRoomType().getMaxAdults() );
                roomTypeForSearchResultDTO.setNoRooms( contractsRoomTypesRelation.getNoRooms() );
                roomTypeForSearchResultDTO.setPrice( (contractsRoomTypesRelation.getPrice() * (1+ ((float)contractsRoomTypesRelation.getContract().getMarkup())/100)) );
                roomTypeForSearchResultDTO.setType( contractsRoomTypesRelation.getRoomType().getType() );

                searchResultDTO.addRoom( roomTypeForSearchResultDTO );

                searchResultDTOS.add(searchResultDTO);
            }

        }

        return searchResultDTOS;
    }


    private List<SearchResultDTO> updateIsPossible(List<SearchResultDTO> searchResultDTOS, List<Integer> numberOfAdults, boolean selector){
        HashMap<Integer, Integer> inputValue = countIntegers( numberOfAdults );
        List<SearchResultDTO> valid = new ArrayList<>();
        for( SearchResultDTO resultDTO:
             searchResultDTOS )
        {
            List<RoomTypeForSearchResultDTO> roomTypeForSearchResultDTOS  = resultDTO.getRooms();

            HashMap<Integer,Integer> hotelRooms = countRooms( roomTypeForSearchResultDTOS );

            boolean possibility = isPossible( inputValue,hotelRooms );
            resultDTO.setPossible( possibility);

            if (selector == possibility){
                valid.add( resultDTO );
            }
        }
        return valid;
    }

    private HashMap<Integer, Integer> countIntegers(List<Integer> integerList){
        HashMap<Integer, Integer> hashMap = fillHashMap( new HashMap<>() );

        ListIterator<Integer> iteratorInput = integerList.listIterator();
        while( iteratorInput.hasNext() ){
            int integer = iteratorInput.next();
            int currentVal = hashMap.get( integer );
            hashMap.put( integer, currentVal +1);
        }

        return hashMap;
    }

    private HashMap<Integer, Integer> fillHashMap(HashMap<Integer,Integer> hashMap){
        for( int i = 0; i < MAX_ADULTS ; i++ )
        {
            hashMap.put( i+1,0 );
        }
        return hashMap;
    }

    private HashMap<Integer, Integer> countRooms(List<RoomTypeForSearchResultDTO> roomTypeForSearchResultDTOS){
        HashMap<Integer, Integer> roomCount = fillHashMap( new HashMap<>() );

        ListIterator<RoomTypeForSearchResultDTO> iterator
                = roomTypeForSearchResultDTOS.listIterator();

        while( iterator.hasNext() ){
            RoomTypeForSearchResultDTO roomType
                    = iterator.next();
            int maxAdults = roomType.getMaxAdults();
            int noRooms = roomType.getNoRooms();

            int currentRooms = roomCount.get( maxAdults );

            roomCount.put( maxAdults, currentRooms + noRooms );
        }
        return roomCount;
    }

    private boolean isPossible(HashMap<Integer, Integer> inputValue, HashMap<Integer, Integer> hotelRooms){
        for( int i = 0; i < MAX_ADULTS ; i++ ){
            if(inputValue.get( i+1 )> hotelRooms.get( i+1 )){
                return false;
            }
        }
        return true;
    }
}
