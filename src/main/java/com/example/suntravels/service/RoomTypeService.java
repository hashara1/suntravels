package com.example.suntravels.service;

import com.example.suntravels.dto.RoomTypeDTO;
import com.example.suntravels.model.Hotel;
import com.example.suntravels.model.RoomType;
import com.example.suntravels.repository.RoomTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoomTypeService
{

    @Autowired
    private RoomTypeRepository roomTypeRepository;

    public List<RoomTypeDTO> getAllRoomTypesOfHotel(Hotel hotel)
    {
        return ( roomTypeRepository.findAllByHotel( hotel ) ).stream().map( this::roomTypeToRoomTypeDTO )
                                            .collect( Collectors.toList() );

    }


    private RoomTypeDTO roomTypeToRoomTypeDTO( RoomType roomType ){
        RoomTypeDTO roomTypeDTO = new RoomTypeDTO();

        roomTypeDTO.setRoomTypeId( roomType.getRoomTypeId() );
        roomTypeDTO.setType( roomType.getType() );
        roomTypeDTO.setMaxAdults( roomType.getMaxAdults() );

        return roomTypeDTO;

    }
}
