package com.example.suntravels.service;


import com.example.suntravels.dto.HotelDTO;
import com.example.suntravels.model.Hotel;
import com.example.suntravels.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class HotelService
{
    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    private RoomTypeService roomTypeService;

    public List<HotelDTO> getAllHotels()
    {
        return ( hotelRepository.findAll() ).stream().map( this::hotelToHotelWithRoomTypeDTO )
                                            .collect( Collectors.toList() );

    }

    private HotelDTO hotelToHotelWithRoomTypeDTO( Hotel hotel ){
        HotelDTO hotelDTO = new HotelDTO();

        hotelDTO.setHotelId( hotel.getHotelId() );
        hotelDTO.setEmail( hotel.getEmail() );
        hotelDTO.setLocation( hotel.getLocation() );
        hotelDTO.setName( hotel.getName() );
        hotelDTO.setTelephone( hotel.getTelephone() );
        hotelDTO.setType( hotel.getType() );
        hotelDTO.setRoomTypes( roomTypeService.getAllRoomTypesOfHotel( hotel ) );

        return hotelDTO;
    }


}
