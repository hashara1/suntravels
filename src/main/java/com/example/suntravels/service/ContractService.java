package com.example.suntravels.service;

import com.example.suntravels.dto.ContractDTO;
import com.example.suntravels.dto.RoomTypeForContractDTO;
import com.example.suntravels.model.Contract;
import com.example.suntravels.model.ContractsRoomTypesKey;
import com.example.suntravels.model.ContractsRoomTypesRelation;
import com.example.suntravels.model.Hotel;
import com.example.suntravels.model.RoomType;
import com.example.suntravels.repository.ContractRepository;
import com.example.suntravels.repository.ContractsRoomTypesRelationRepository;
import com.example.suntravels.repository.HotelRepository;
import com.example.suntravels.repository.RoomTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ContractService
{
    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    private RoomTypeRepository roomTypeRepository;

    @Autowired
    private ContractsRoomTypesRelationRepository contractsRoomTypesRelationRepository;



    public List<ContractDTO> getAllContracts() {
        return ( contractRepository.findAll()).stream().map(this::convertToAddContractDTO)
                                              .collect( Collectors.toList());
    }

    public void addContract( ContractDTO contractDTO ){

        UUID hotelId = contractDTO.getHotel().getHotelId();
        UUID contractId = UUID.randomUUID();

        List<RoomType> roomTypes = new ArrayList<>();
        List<ContractsRoomTypesRelation> contractsRoomTypesRelations = new ArrayList<>();

        Contract contract  = new Contract();
        contract.setContractId( contractId );
        contract.setStartDate( contractDTO.getStartDate() );
        contract.setEndDate( contractDTO.getEndDate() );
        contract.setMarkup( contractDTO.getMarkup() );

        if (hotelId.equals( new UUID( 0,0 ) )){

            Hotel hotel = contractDTO.getHotel();
            hotel.setHotelId( UUID.randomUUID() );
            contract.setHotel( hotel );

            for( RoomTypeForContractDTO roomTypeForContractDTO :
                  contractDTO.getRoomTypes())
            {
                RoomType roomType = new RoomType();

                UUID roomTypeId = UUID.randomUUID();
                roomType.setRoomTypeId( roomTypeId );
                roomType.setMaxAdults( roomTypeForContractDTO.getMaxAdults() );
                roomType.setHotel( hotel );
                roomType.setType( roomTypeForContractDTO.getType() );

                roomTypes.add( roomType );


                ContractsRoomTypesRelation contractsRoomTypesRelation
                        = new ContractsRoomTypesRelation( new ContractsRoomTypesKey(contractId,roomTypeId),contract,
                        roomType, roomTypeForContractDTO.getPrice(), roomTypeForContractDTO.getNoRooms());

                contractsRoomTypesRelations.add( contractsRoomTypesRelation );

            }


            contractsRoomTypesRelationRepository.saveAll( contractsRoomTypesRelations );

        }

        else {

            Hotel hotel = hotelRepository.findById(hotelId)
                               .orElseThrow(() -> new  ResponseStatusException(
                                       HttpStatus.NOT_FOUND, "Hotel not found"
                               ));

            contract.setHotel( hotel );

            for( RoomTypeForContractDTO roomTypeForContractDTO :
                    contractDTO.getRoomTypes())
            {

                RoomType roomType;
                UUID roomTypeId;
                if (!roomTypeForContractDTO.getRoomTypeId().equals( new UUID( 0,0 ))){
                    roomType = roomTypeRepository.findById( roomTypeForContractDTO.getRoomTypeId() )
                                                         .orElseThrow(() -> new  ResponseStatusException(
                                                                HttpStatus.NOT_FOUND, "room not found"
                                                        ));

                    if (!roomType.getHotel().equals( hotel )){
                        throw new  ResponseStatusException(
                                HttpStatus.FORBIDDEN, "room is not allowed"
                        );
                    }

                    roomTypeId =  roomTypeForContractDTO.getRoomTypeId();

                }
                else{
                    roomType = new RoomType();

                    roomTypeId = UUID.randomUUID();
                    roomType.setRoomTypeId( roomTypeId );
                    roomType.setMaxAdults( roomTypeForContractDTO.getMaxAdults() );
                    roomType.setHotel( hotel );
                    roomType.setType( roomTypeForContractDTO.getType() );

                    roomTypes.add( roomType );

                }



                ContractsRoomTypesRelation contractsRoomTypesRelation
                        = new ContractsRoomTypesRelation( new ContractsRoomTypesKey(contractId,roomTypeId),contract,
                        roomType, roomTypeForContractDTO.getPrice(), roomTypeForContractDTO.getNoRooms());


                contractsRoomTypesRelations.add( contractsRoomTypesRelation );


            }

            contractsRoomTypesRelationRepository.saveAll( contractsRoomTypesRelations );

        }

    }


    private ContractDTO convertToAddContractDTO( Contract contract){
        ContractDTO contractDTO = new ContractDTO();

        contractDTO.setContractId( contract.getContractId() );
        contractDTO.setHotel( contract.getHotel() );
        contractDTO.setStartDate( contract.getStartDate() );
        contractDTO.setEndDate( contract.getEndDate() );
        contractDTO.setMarkup( contract.getMarkup() );
        contractDTO.setRoomTypes( roomTypeToRoomTypeDTO(
                contractsRoomTypesRelationRepository.findAllByContract( contract )) );

        return contractDTO;
    }

    private List<RoomTypeForContractDTO> roomTypeToRoomTypeDTO(
            List<ContractsRoomTypesRelation> contractsRoomTypesRelations){

        List<RoomTypeForContractDTO> roomTypeForContractDTOS = new ArrayList<>();

        for( ContractsRoomTypesRelation relation:
                contractsRoomTypesRelations )
        {

            RoomTypeForContractDTO roomTypeForContractDTO = new RoomTypeForContractDTO();

            roomTypeForContractDTO.setRoomTypeId( relation.getRoomType().getRoomTypeId() );
            roomTypeForContractDTO.setMaxAdults( relation.getRoomType().getMaxAdults() );
            roomTypeForContractDTO.setNoRooms( relation.getNoRooms() );
            roomTypeForContractDTO.setPrice( relation.getPrice() );
            roomTypeForContractDTO.setType( relation.getRoomType().getType() );

            roomTypeForContractDTOS.add( roomTypeForContractDTO );
        }

        return roomTypeForContractDTOS;


    }

//    public List<Contract> getValidContracts( Date checkingDate, int numberOfNights ){
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime( checkingDate );
//        calendar.add( calendar.DATE,numberOfNights-1 );
//        Date lastDate = calendar.getTime();
//        System.out.println(lastDate);
//        return contractRepository.queryAllByEndDateAfter(lastDate);
//    }
}
