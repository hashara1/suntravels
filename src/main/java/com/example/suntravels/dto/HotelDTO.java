package com.example.suntravels.dto;

import java.util.List;
import java.util.UUID;

public class HotelDTO
{
    private UUID hotelId;
    private String type;
    private String name;
    private String location;
    private String telephone;
    private String email;
    private List<RoomTypeDTO> roomTypes;

    public UUID getHotelId()
    {
        return hotelId;
    }

    public void setHotelId( UUID hotelId )
    {
        this.hotelId = hotelId;
    }

    public String getType()
    {
        return type;
    }

    public void setType( String type )
    {
        this.type = type;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation( String location )
    {
        this.location = location;
    }

    public String getTelephone()
    {
        return telephone;
    }

    public void setTelephone( String telephone )
    {
        this.telephone = telephone;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail( String email )
    {
        this.email = email;
    }

    public List<RoomTypeDTO> getRoomTypes()
    {
        return roomTypes;
    }

    public void setRoomTypes( List<RoomTypeDTO> roomTypes )
    {
        this.roomTypes = roomTypes;
    }
}
