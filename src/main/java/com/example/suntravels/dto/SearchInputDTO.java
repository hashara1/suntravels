package com.example.suntravels.dto;

import java.util.Date;
import java.util.List;

public class SearchInputDTO
{
    private Date checkingDate;
    private int noOfNights;
    private List<RoomAdultsForSearchInputDTO> roomsAdults;

    public Date getCheckingDate()
    {
        return checkingDate;
    }

    public void setCheckingDate( Date checkingDate )
    {
        this.checkingDate = checkingDate;
    }

    public int getNoOfNights()
    {
        return noOfNights;
    }

    public void setNoOfNights( int noOfNights )
    {
        this.noOfNights = noOfNights;
    }

    public List<RoomAdultsForSearchInputDTO> getRoomsAdults()
    {
        return roomsAdults;
    }

    public void setRoomsAdults( List<RoomAdultsForSearchInputDTO> roomsAdults )
    {
        this.roomsAdults = roomsAdults;
    }
}


