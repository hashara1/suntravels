package com.example.suntravels.dto;

import com.example.suntravels.model.ContractsRoomTypesKey;
import com.example.suntravels.model.Hotel;

import java.util.ArrayList;
import java.util.List;

public class SearchResultDTO
{
    private Hotel hotel;
    private List<RoomTypeForSearchResultDTO> rooms = new ArrayList<>();
    private boolean isPossible = false;

    public boolean isPossible()
    {
        return isPossible;
    }

    public void setPossible( boolean possible )
    {
        isPossible = possible;
    }

    public Hotel getHotel()
    {
        return hotel;
    }

    public void setHotel( Hotel hotel )
    {
        this.hotel = hotel;
    }

    public List<RoomTypeForSearchResultDTO> getRooms()
    {
        return rooms;
    }

    public void setRooms( List<RoomTypeForSearchResultDTO> rooms )
    {
        this.rooms = rooms;
    }

    public void addRoom(RoomTypeForSearchResultDTO roomTypeForSearchResultDTO){
        this.rooms.add( roomTypeForSearchResultDTO );
    }
}
