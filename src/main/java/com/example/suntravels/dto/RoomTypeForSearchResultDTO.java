package com.example.suntravels.dto;

import com.example.suntravels.model.ContractsRoomTypesKey;

public class RoomTypeForSearchResultDTO
{
    private ContractsRoomTypesKey contractsRoomTypesKey;
    private String type;
    private int maxAdults;
    private float price;
    private int noRooms;

    public ContractsRoomTypesKey getContractsRoomTypesKey()
    {
        return contractsRoomTypesKey;
    }

    public void setContractsRoomTypesKey( ContractsRoomTypesKey contractsRoomTypesKey )
    {
        this.contractsRoomTypesKey = contractsRoomTypesKey;
    }

    public String getType()
    {
        return type;
    }

    public void setType( String type )
    {
        this.type = type;
    }

    public int getMaxAdults()
    {
        return maxAdults;
    }

    public void setMaxAdults( int maxAdults )
    {
        this.maxAdults = maxAdults;
    }

    public float getPrice()
    {
        return price;
    }

    public void setPrice( float price )
    {
        this.price = price;
    }

    public int getNoRooms()
    {
        return noRooms;
    }

    public void setNoRooms( int noRooms )
    {
        this.noRooms = noRooms;
    }
}
