package com.example.suntravels.dto;

import com.example.suntravels.model.Hotel;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class ContractDTO
{
    private UUID contractId;
    private Hotel hotel;
    private Date startDate;
    private Date endDate;
    private double markup;
    private List<RoomTypeForContractDTO> roomTypes;


    public UUID getContractId()
    {
        return contractId;
    }

    public void setContractId( UUID contractId )
    {
        this.contractId = contractId;
    }

    public Hotel getHotel()
    {
        return hotel;
    }

    public void setHotel( Hotel hotel )
    {
        this.hotel = hotel;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate( Date startDate )
    {
        this.startDate = startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate( Date endDate )
    {
        this.endDate = endDate;
    }

    public double getMarkup()
    {
        return markup;
    }

    public void setMarkup( double markup )
    {
        this.markup = markup;
    }

    public List<RoomTypeForContractDTO> getRoomTypes()
    {
        return roomTypes;
    }

    public void setRoomTypes( List<RoomTypeForContractDTO> roomTypes )
    {
        this.roomTypes = roomTypes;
    }
}
