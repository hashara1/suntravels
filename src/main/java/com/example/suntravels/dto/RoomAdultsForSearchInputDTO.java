package com.example.suntravels.dto;

public class RoomAdultsForSearchInputDTO
{
    private int numberOfAdults;

    public int getNumberOfAdults()
    {
        return numberOfAdults;
    }

    public void setNumberOfAdults( int numberOfAdults )
    {
        this.numberOfAdults = numberOfAdults;
    }
}
