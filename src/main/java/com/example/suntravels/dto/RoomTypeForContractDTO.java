package com.example.suntravels.dto;


import java.util.UUID;

public class RoomTypeForContractDTO
{
    private UUID roomTypeId = new UUID( 0,0 );
    private String type;
    private int maxAdults;
    private float price;
    private int noRooms;

    public UUID getRoomTypeId()
    {
        return roomTypeId;
    }

    public void setRoomTypeId( UUID roomTypeId )
    {
        this.roomTypeId = roomTypeId;
    }

    public String getType()
    {
        return type;
    }

    public void setType( String type )
    {
        this.type = type;
    }

    public int getMaxAdults()
    {
        return maxAdults;
    }

    public void setMaxAdults( int maxAdults )
    {
        this.maxAdults = maxAdults;
    }

    public float getPrice()
    {
        return price;
    }

    public void setPrice( float price )
    {
        this.price = price;
    }

    public int getNoRooms()
    {
        return noRooms;
    }

    public void setNoRooms( int noRooms )
    {
        this.noRooms = noRooms;
    }
}
