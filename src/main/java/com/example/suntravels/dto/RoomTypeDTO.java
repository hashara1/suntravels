package com.example.suntravels.dto;

import java.util.UUID;

public class RoomTypeDTO
{
    private UUID roomTypeId;
    private String type;
    private int maxAdults;

    public UUID getRoomTypeId()
    {
        return roomTypeId;
    }

    public void setRoomTypeId( UUID roomTypeId )
    {
        this.roomTypeId = roomTypeId;
    }

    public String getType()
    {
        return type;
    }

    public void setType( String type )
    {
        this.type = type;
    }

    public int getMaxAdults()
    {
        return maxAdults;
    }

    public void setMaxAdults( int maxAdults )
    {
        this.maxAdults = maxAdults;
    }

}
